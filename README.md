# Shinken pack for BackupPC

[Shinken](http://www.shinken-monitoring.org/) [pack](http://shinken.io/package/backuppc) to check [BackupPC](http://backuppc.com/) status through SSH (`check_by_ssh`).

## Requirements

On the BackupPC host, you must:
- Install your OS package for `libnagios-plugin-perl`, in order to have Nagios
  Plugins libraries `/usr/lib/nagios/plugins/utils.pm`;
- Deploy the included `check_backuppc` Perl script on each BackupPC servers. By
  default, it's expected to be found at the root of the user `backuppc`'s home:
  `~backuppc/check_backuppc`.

## Usage

Usage example:

```
$ /usr/lib/nagios/plugins/check_by_ssh -H 10.0.0.42 -p 22 -l backuppc -C ./check_backuppc
BACKUPPC OK - (0/2) failures
```

# Thanks

**Tetragon** for writing the original
[check_backuppc](http://sourceforge.net/projects/n-backuppc/) Perl script.

